package com.exemple.controller;


import com.exemple.model.Book;
import com.exemple.repository.BookRepository;
import com.exemple.service.AWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController("/api/book")
public class BookController {

    @Autowired
    BookRepository bookRepository;


    @Autowired
    AWS aws;

    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public Iterable<Book> list() throws IOException {
        aws.connectAws();
        return bookRepository.findAll();
    }

    @GetMapping("/titulos")
    public Iterable<Book> get(@RequestParam String id) {
       return entityManager
                .createQuery("SELECT b.title FROM Book b where b.id = '"+id+"'") // Week point
                .getResultList();
    }

    @GetMapping("/capa")
    public @ResponseBody byte[] getImage(@RequestParam String imagem) throws IOException {
        File file = new File("resources/images/", imagem); //Weak point
        return Files.readAllBytes(file.toPath());
    }



}
